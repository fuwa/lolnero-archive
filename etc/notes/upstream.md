dev
=========

v0.6.4.0
=========

@cb70ae9

skipped:

* fbb03ef

skipped, maybe later:

* 7f30c49 simplewallet: don't complain about connecting to the daemon when offline
* bcdc6c6 protocol: fix false positives dropping peers
* 1eb14af rpc: limit the number of txes for get_blocks.bi
* c5c278c p2p: only log to global when a blocked IP is not already blocked

v0.6.2.0
========

@e5decd0

skipped:

* 7414e2b
* 08eb094


v0.6.1.4
========

@b7425c1

