## [ANN] Lolnero - much privacy! many ASICs!

Lolnero is a fork of Lolnero with a linear emission and a SHA-3 PoW.

There is no premine and no dev tax.

Source code: https://gitlab.com/fuwa/lolnero (in development)

Lolnero will be launched on one of the following dates:

* Oct 10th, 2020
* Nov 11th, 2020
* Dev 12th, 2020

You can vote the launch date here:

https://twitter.com/101n3r0/status/1313197529027112960
